package com.epam.repository.repositoryImpl;

import com.epam.dto.Product;
import com.epam.repository.ProductRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ProductRepositoryImpl implements ProductRepository {

    private List<Product> products = new ArrayList<>();

    @Override
    public void addProduct(Product product) {
        products.add(product);
    }

    @Override
    public Product getProductById(long id) {
        return products.stream().filter(product -> product.getId() == id).findFirst().get();
    }

    @Override
    public List<Product> getProductsByName(String name) {
        return products.stream()
                .filter(product -> product.getName().equals(name))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteProductById(long id) {

    }

    @Override
    public List<Product> getAllProducts() {
        return products;
    }
}

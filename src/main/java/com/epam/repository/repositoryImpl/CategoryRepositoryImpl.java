package com.epam.repository.repositoryImpl;

import com.epam.dto.Category;
import com.epam.repository.CategoryRepository;

import java.util.ArrayList;
import java.util.List;

public class CategoryRepositoryImpl implements CategoryRepository {

    private List<Category> categories = new ArrayList<>();

    @Override
    public void addCategory(Category category) {
        categories.add(category);
    }

    @Override
    public List<Category> getAllCategories() {
        return categories;
    }
}

package com.epam.repository;

import com.epam.dto.Category;
import com.epam.dto.Product;

import java.util.List;

public interface CategoryRepository {

    public void addCategory(Category category);

    public List<Category> getAllCategories();
}

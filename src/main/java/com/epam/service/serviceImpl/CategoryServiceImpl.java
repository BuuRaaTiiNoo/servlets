package com.epam.service.serviceImpl;

import com.epam.dto.Category;

import com.epam.repository.CategoryRepository;
import com.epam.repository.repositoryImpl.CategoryRepositoryImpl;
import com.epam.service.CategoryService;

import java.util.List;

public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository = new CategoryRepositoryImpl();

    @Override
    public void addCategory(Category category) {
        categoryRepository.addCategory(category);
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.getAllCategories();
    }
}

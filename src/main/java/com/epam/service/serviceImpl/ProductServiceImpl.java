package com.epam.service.serviceImpl;

import com.epam.dto.Product;
import com.epam.repository.ProductRepository;
import com.epam.repository.repositoryImpl.ProductRepositoryImpl;
import com.epam.service.ProductService;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    private ProductRepository productRepository = new ProductRepositoryImpl();

    @Override
    public void addProduct(Product product) {
        productRepository.addProduct(product);
    }

    @Override
    public Product getProductById(long id) {
        return productRepository.getProductById(id);
    }

    @Override
    public List<Product> getProductsByName(String name) {
        return productRepository.getProductsByName(name);
    }

    @Override
    public void deleteProductById(long id) {

    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }
}

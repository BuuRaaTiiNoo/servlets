package com.epam.service;

import com.epam.dto.Category;
import com.epam.dto.Product;

import java.util.List;

public interface CategoryService {

    public void addCategory(Category category);

    public List<Category> getAllCategories();
}

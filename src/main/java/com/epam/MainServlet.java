package com.epam;

import com.epam.generate.Generate;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@WebServlet({"/", "/index"})
public class MainServlet extends HttpServlet {

    private Generate generate = new Generate();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute("login") == null)
            resp.sendRedirect("/authentication");
        else {
            req.setAttribute("categories", generate.getCategoryService().getAllCategories());
            req.getRequestDispatcher("jsp/index.jsp").forward(req, resp);
        }
    }
}



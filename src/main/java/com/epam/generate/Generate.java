package com.epam.generate;

import com.epam.dto.Category;
import com.epam.dto.Product;
import com.epam.service.CategoryService;
import com.epam.service.ProductService;
import com.epam.service.serviceImpl.CategoryServiceImpl;
import com.epam.service.serviceImpl.ProductServiceImpl;

public class Generate {

    private ProductService productService = new ProductServiceImpl();

    private CategoryService categoryService = new CategoryServiceImpl();

    public Generate() {
        productService.addProduct(new Product("qwe", "100", "description"));
        productService.addProduct(new Product("asd", "100", "description"));
        productService.addProduct(new Product("zxc", "100", "description"));
        productService.addProduct(new Product("rty", "100", "description"));
        productService.addProduct(new Product("fgh", "100", "description"));
        productService.addProduct(new Product("vbn", "100", "description"));

        categoryService.addCategory(new Category("FirstCategory", productService.getAllProducts()));
        categoryService.addCategory(new Category("SecondCategory", productService.getAllProducts()));
        categoryService.addCategory(new Category("ThirdCategory", productService.getAllProducts()));
        categoryService.addCategory(new Category("LastCategory", productService.getAllProducts()));
    }

    public CategoryService getCategoryService() {
        return categoryService;
    }
}

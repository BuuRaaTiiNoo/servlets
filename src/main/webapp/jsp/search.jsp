<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<div class="modal" id="search">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100">Поиск</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body bodysearch text-center">
                    <div class="row p-3">
                        <div class="col"></div>
                        <div class="col-5">
                            <div class="p-3">
                                <form class="form-group" action="#">
                                    <input class="form-control" type="text" placeholder="Search" id="searchform">
                                </form>
                            </div>
                        </div>
                        <div class="col"></div>
                    </div>
                    <button type="submit" class="btn btn-primary search" id="searchbtn">Найти</button>
                    <div class="bodycontent">
                        <div class="container-fluid section">
                            <div class="d-flex flex-wrap bg-default">
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
            </div>

     </div>
 </div>
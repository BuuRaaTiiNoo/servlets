<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@include file="header.jsp"%>

<%@include file="navbar.jsp"%>

    <div class="content">
        <c:forEach items="${categories}" var="category">
            <div class="category">
                <div class="sticky-top">
                    <h3>${category.name}</h3>
                </div>
                <div class="container-fluid section" id="section">
                    <div class="d-flex flex-wrap bg-default">
                        <c:forEach items="${category.products}" var="product">
                            <div class="p-1 border">
                                <div class="card">
                                    <div class="card-body">
                                        <div>
                                            <img src="https://placehold.it/150x80?text=IMAGE" class="img-responsive" alt="Image">
                                        </div>
                                        <div>
                                            <p class="card-text">${product.name}</p>
                                        </div>
                                        <div class="description" style="display: none;">
                                            <div class="cost" style="display: inline; margin: 0">${product.cost}
                                            </div>
                                            <span>Rub</span>
                                            <button type="button" class="btn btn-default add-bucket">В корзину</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>

    <%@include file="bucket.jsp"%>
    <%@include file="search.jsp"%>
<%@include file="footer.jsp"%>


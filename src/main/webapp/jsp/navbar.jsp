<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<nav class="navbar navbar-expand-sm navbar-light bg-light fixed-top">
        <a class="navbar-brand" href="">Online Shop</a>
        <div class="navbar-collapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#section1">Section 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#section2">Section 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#section3">Section 3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#section4">Section 4</a>
                </li>
            </ul>
            <button type="button" class="btn btn-light modal-search" data-toggle="modal" data-target="#search">
                Search
            </button>
            <button type="button" class="btn btn-light modal-buy" data-toggle="modal" data-target="#bucket">
                Buy
                <span class="badge badge-secondary"></span>
            </button>
            <a href="authentication">
                <button type="button" class="btn btn-light login">Login</button>
            </a>
        </div>
    </nav>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<div class="modal" id="bucket">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100">Корзина</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="content-bucket">
                    </div>
                    <div class="row">
                        <div class="col-3">
                            Сумма товаров
                        </div>
                        <div class="col-3">
                            <p class="summ"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            Скидка
                        </div>
                        <div class="col-3">
                            <p class="discount"></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            Итого
                        </div>
                        <div class="col-3">
                            <p class="result"></p>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Оформить</button>
                </div>
            </div>
        </div>
    </div>